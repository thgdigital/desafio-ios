//
//  HeaderCell.swift
//  Desafio
//
//  Created by Thiago Santos on 27/09/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit

class HeaderCell: BaseViewCell {
    @IBOutlet weak var openLabel: UILabel!
    @IBOutlet weak var closeLabel: UILabel!
    
    var gitHub: GitHub? {
        didSet {
           
            if let open = gitHub?.openCount{
                openLabel.text = "\(open) opened"
            }
        }
    }
    
    override func setupViews() {
        
        super.setupViews()
        self.backgroundColor = UIColor.clear
        
    }

}
