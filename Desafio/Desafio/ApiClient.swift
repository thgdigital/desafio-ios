//
//  ApiClient.swift
//  Desafio
//
//  Created by Thiago Santos on 27/09/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ApiClient {
    
    
   static let shared = ApiClient()
    
     public typealias RequestCompletion = (_ response: HTTPURLResponse?, _ json: JSON?, _ error: Error?) -> Void
    
     func getRepsitoryFromPage(_ page: Int , completionHandler: @escaping (_ result: ([GitHub]), _ nextPage: String?, _ status: Bool) -> ()) {
        let urlString = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)"
        getRepositoryFromURL(urlString, completionHandler: completionHandler)
    }
    
    fileprivate  func getNextPageFromHeaders(_ headers: [AnyHashable: Any]?) -> String? {
        let link = headers?["Link"] as? NSString
        // TODO: Refatorar
        return link?.components(separatedBy: ";").first?.replacingOccurrences(of: "<", with: "").replacingOccurrences(of: ">", with: "")
    }
    
    
     func getRepositoryFromURL(_ urlString: String , completionHandler: @escaping (_ result: ([GitHub]), _ nextPage: String?, _ status: Bool) -> ()) {
        
     
        var next: String?
        _ = self.requestGET(url: urlString, completion: { (response, json, error) in
            if let _ = error{
                completionHandler([], nil, false)
                return
            }
            next =  self.getNextPageFromHeaders(response?.allHeaderFields)
            if let  gitHubs = json?["items"].arrayValue.map({return GitHub(json: $0)}){
                 completionHandler(gitHubs, next ,true)
                return
            }
             completionHandler([], nil, false)
        })
       
    }
    
    
    func requestGET(url: String, completion: @escaping RequestCompletion) -> Request? {
        
        return  Alamofire.request(url)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    if let jsonObject = response.result.value {
                        let json = JSON(jsonObject)
                      DispatchQueue.main.async {
                            () -> Void in
                            completion(response.response, json, nil)
                        }
                        
                    }
                case let .failure(error):
                     completion(response.response, nil, error)
                    
                }
        }
    }

     func getPull(_ nameRepositorio: String ,username: String ,  completionHandler: @escaping (_ result: ([Pull]), _ status: Bool) -> ()) {
        let urlString = "https://api.github.com/repos/\(username)/\(nameRepositorio)/pulls"
        
        _ = self.requestGET(url: urlString, completion: { (response, json, error) in
            if let _ = error{
                completionHandler([], false)
                return
            }
            if let   pulls = json?.arrayValue.map({return Pull(json: $0) }){
                completionHandler(pulls,true)
                return
            }
            completionHandler([], false)
        
        })
    }

}
