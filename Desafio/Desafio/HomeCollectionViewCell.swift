//
//  HomeCollectionViewCell.swift
//  Desafio
//
//  Created by Thiago Santos on 26/09/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit
import Kingfisher

class HomeCollectionViewCell: BaseViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descritionTextView: UILabel!
    
    @IBOutlet weak var avatarImagemView: UIImageView!
    
    @IBOutlet weak var forksImagemView: UIImageView!
    @IBOutlet weak var forksLabel: UILabel!
    
    @IBOutlet weak var startImagemView: UIImageView!
    
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    
    
    
    var gitHub: GitHub? {
        didSet {
            if let descriptRepo = gitHub?.descris{
                descritionTextView.text = descriptRepo
            }
            if let title = gitHub?.nameReposi{
                    titleLabel.text = title
            }
            if let urlImage = gitHub?.avatar{
                
                let url = URL(string: urlImage)
                
                let resource = ImageResource(downloadURL: url!, cacheKey: "desafio")
                avatarImagemView.kf.setImage(with: resource)
                avatarImagemView.layer.cornerRadius = avatarImagemView.frame.height / 2
                avatarImagemView.layer.masksToBounds = true
                avatarImagemView.contentMode = .scaleAspectFill
                
            }
            if let forksNumber = gitHub?.nForks{
                forksLabel.text = "\(forksNumber)"
                forksLabel.textColor = UIColor(red: 221/255, green: 145/255, blue: 28/255, alpha: 1)
                forksImagemView.image = UIImage(named: "source-fork")?.withRenderingMode(.alwaysTemplate)
                forksImagemView.tintColor = UIColor(red: 221/255, green: 145/255, blue: 28/255, alpha: 1)
            }
            if let startNumeber = gitHub?.nStart{
                startLabel.text = "\(startNumeber)"
                startLabel.textColor = UIColor(red: 221/255, green: 145/255, blue: 28/255, alpha: 1)
                startImagemView.image = UIImage(named: "star")?.withRenderingMode(.alwaysTemplate)
                startImagemView.tintColor = UIColor(red: 221/255, green: 145/255, blue: 28/255, alpha: 1)
                
            }
            if let userName = gitHub?.userName{
                userNameLabel.text = userName
            }
        }
    }
    override func setupViews() {
        super.setupViews()
        backgroundColor = UIColor.clear
       
        forksImagemView?.image = UIImage(named: "start")?.withRenderingMode(.alwaysTemplate)
        forksImagemView?.tintColor = UIColor.yellow

        
    }
    
}
