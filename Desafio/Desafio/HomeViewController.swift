//
//  ViewController.swift
//  Desafio
//
//  Created by Thiago Santos on 23/09/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit
import DZNEmptyDataSet


class HomeViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate  {
    fileprivate let cellId = "cellId"
    let footerReuseIdentifier: String = "footer"
    
    // MARK: - Controles da Paginação
    fileprivate var nextPageURL: String?
    fileprivate var loadingItens: Bool = false {
        didSet {
            self.collectionViewLayout.invalidateLayout()
        }
    }
    
    
    // MARK: - Loader da Paginação
    let activityView = UIActivityIndicatorView()
   
    var  git = [GitHub]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        collectionView?.backgroundColor = UIColor.white
        //collectionView?.registerClass(HomeCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        self.navigationItem.title = "Github JavaPop"
        self.collectionView!.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter , withReuseIdentifier: footerReuseIdentifier)
        
        activityView.color = UIColor.black
        
        
        self.collectionView?.emptyDataSetSource = self
        self.collectionView?.emptyDataSetSource = self

        getRepo(1)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getRepo(_ page: Int){
        
        self.loadingItens = true
        
         ApiClient.shared.getRepsitoryFromPage(page){ (result, nextPage, status) in
            
            self.loadingItens = false
            if status == true {
                self.nextPageURL = nextPage
                self.git.append(contentsOf: result)
                self.collectionView?.reloadData()
            }
        }
    }
    
    fileprivate func fetchMoreItemsIfNeeded() {
        // Tem próxima página
        if let nextPage = self.nextPageURL, !self.loadingItens {
            //TODO: Validar falta de conectividade
            
            self.loadingItens = true
             ApiClient.shared.getRepositoryFromURL(nextPage, completionHandler: { (result, nextPage, status) in
                
                self.loadingItens = false

                if status == true {
                    self.nextPageURL = nextPage
                    self.git.append(contentsOf: result)
                    self.collectionView?.reloadData()
                }
            })
            
        }
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 150)
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      
        return self.git.count
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showDetais", sender: self)
        
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! HomeCollectionViewCell
        
        cell.gitHub = git[indexPath.item]
        
        return cell
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == "showDetais" {
            let indexPaths = self.collectionView?.indexPathsForSelectedItems!
            let indexPath = indexPaths![0] as IndexPath
            let vc = segue.destination as! DetailsViewController
            vc.github = git[indexPath.item]
        }
    }
    
    
    // MARK: - UIScrollViewDelegate
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Request
        if scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.bounds.height {
            self.fetchMoreItemsIfNeeded()
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let footerView: UICollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerReuseIdentifier, for: indexPath)
        
        if self.loadingItens && (self.nextPageURL != nil || self.git.count == 0) {
            footerView.clipsToBounds = true
            self.activityView.frame.origin.x = (footerView.frame.width / 2) - self.activityView.frame.width/2
            self.activityView.frame.origin.y = footerView.frame.height / 2 - self.activityView.frame.height/2
            
            footerView.addSubview(self.activityView)
            self.activityView.startAnimating()
        } else {
            self.activityView.removeFromSuperview()
            //            footerView.frame = CGRectZero
        }
        
        return footerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if self.loadingItens && (self.nextPageURL != nil || self.git.count == 0)  {
            return CGSize(width: view.frame.width, height: 40);
        }
        return CGSize(width: view.frame.width, height: 1);
    }
}

