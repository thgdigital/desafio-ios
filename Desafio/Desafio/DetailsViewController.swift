//
//  DetailsViewController.swift
//  Desafio
//
//  Created by Thiago Santos on 26/09/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit
import RNActivityView

class DetailsViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    fileprivate let cellId = "cellIds"
    fileprivate let cellidHeader = "cellidHeader"
    let footerReuseIdentifier: String = "footer"
    fileprivate var loadingItens: Bool = false {
        didSet {
            self.collectionViewLayout.invalidateLayout()
        }
    }
    
    
    var github: GitHub? {
        didSet {
            self.view.showActivityView()
             ApiClient.shared.getPull((github?.nameReposi)!, username: (github?.userName)!, completionHandler: { (result, status) in
                if status {
                    self.pull = result
                    self.collectionView?.reloadData()
                }
                self.view.hideActivityView()
            })
            
          collectionView?.reloadData()
        }
    }

    var pull: [Pull]?
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        titleLabel.text = github!.nameReposi
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont.systemFont(ofSize: 20)
        navigationItem.titleView = titleLabel
        view.backgroundColor = UIColor.white
        
      
        collectionView?.reloadData()
    }
 
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = pull?.count{
            return count
        }
        
        return 0
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let cell =  collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: cellidHeader, for: indexPath) as! HeaderCell
        cell.gitHub = github
        return cell
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PullCell
        
        cell.pull = pull?[indexPath.item]
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
         return CGSize(width: view.frame.width, height: 50)
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let urls = pull?[indexPath.item].urlPull{
            let url = URL(string: urls)!
            UIApplication.shared.openURL(url)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 230)
    }
}
