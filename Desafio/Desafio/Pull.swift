//
//  Pull.swift
//  Desafio
//
//  Created by Thiago Santos on 27/09/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


struct  Pull  {
    var id: NSNumber?
    var name: String?
    var title: String?
    var avatar: String?
    var descript: String?
    var data: String?
    var urlPull: String?
    
     init() {
        
    }
    init(json: JSON) {
        self.id = json["id"].number
        self.descript = json["body"].stringValue
        self.avatar = json["user"]["avatar_url"].stringValue
        self.title = json["title"].stringValue
        self.data = json["updated_at"].stringValue
        self.urlPull = json["html_url"].stringValue
        self.name = json["user"]["login"].stringValue
    }
    
   }
