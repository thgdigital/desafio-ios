//
//  GitHub.swift
//  Desafio
//
//  Created by Thiago Santos on 26/09/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

struct GitHub {
    
    var id: NSNumber?
    var avatar: String?
    var nameAutor: String?
    var userName: String?
    var nStart: NSNumber?
    var nForks: NSNumber?
    var descris: String?
    var nameReposi: String?
    var openCount: NSNumber?
    
    init() {
        
    }
    init(json:JSON) {
       
        self.id         = json["owner"]["id"].number
        self.avatar     = json["owner"]["avatar_url"].stringValue
        self.userName   = json["owner"]["login"].stringValue
        self.descris    = json["description"].stringValue
        self.nForks     = json["forks"].number
        self.nStart     = json["stargazers_count"].number
        self.nameReposi = json["name"].stringValue
        self.openCount  = json["open_issues_count"].number
    }
}
