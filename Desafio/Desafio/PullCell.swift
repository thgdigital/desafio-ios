//
//  PullCell.swift
//  Desafio
//
//  Created by Thiago Santos on 27/09/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit

class PullCell: BaseViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptLabel: UILabel!
    @IBOutlet weak var avatarImagemView: UIImageView!
    @IBOutlet weak var dateLabelLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    var pull: Pull?{
        
        
        didSet {
            if let title = pull?.title{
                titleLabel.text = title
            }
            if let descri = pull?.descript{
                descriptLabel.text = descri
            }else{
                descriptLabel.text = ""
            }
            if let urlImage =  pull?.avatar{
                let url = URL(string: urlImage)
                avatarImagemView.kf.setImage(with: url)
                avatarImagemView.layer.cornerRadius = avatarImagemView.frame.height / 2
                avatarImagemView.layer.masksToBounds = true
                avatarImagemView.contentMode = .scaleAspectFill
            }
            if let name = pull?.name{
                nameLabel.text =  name
            }
            if let dateStr = pull?.data {
                let formatter = DateFormatter()
                // 2014-06-17T00:13:54Z
                formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"
                formatter.locale = Locale(identifier: "en_US_POSIX")
                formatter.timeZone = TimeZone(secondsFromGMT: 0)

                
                if let date = formatter.date(from: dateStr) {
                    formatter.dateFormat = "dd/MM/yyyy HH:mm"
                    formatter.string(from: date)
                    dateLabelLabel.text = formatter.string(from: date)
                }
            }
            
        }
    }

    
    
    override func setupViews() {
        super.setupViews()
        
         self.backgroundColor = UIColor.clear
        
     
    }
}
